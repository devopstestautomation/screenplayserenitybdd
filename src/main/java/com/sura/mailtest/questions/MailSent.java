package com.sura.mailtest.questions;

import com.sura.mailtest.user_interfaces.InboxPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Visibility;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.ValueOf.the;

@Subject("El correo enviado")
public class MailSent implements Question<ElementAvailability> {
    @Override
    public ElementAvailability answeredBy(Actor actor) {
        actor.attemptsTo(
                WaitUntil.the(InboxPage.LBLSENTMAIL, isVisible())
        );
        return ElementAvailability.from(the(Visibility.of(InboxPage.LBLSENTMAIL).viewedBy(actor)));
    }

    public static MailSent element() {
        return new MailSent();
    }

}
