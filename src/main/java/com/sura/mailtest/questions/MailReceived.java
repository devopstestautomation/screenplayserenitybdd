package com.sura.mailtest.questions;

import com.sura.mailtest.user_interfaces.InboxPage;
import com.sura.mailtest.util.Util;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Visibility;

import static net.serenitybdd.screenplay.questions.ValueOf.the;

@Subject("El correo recibido")
public class MailReceived implements Question<ElementAvailability> {

    private InboxPage inboxPage = new InboxPage();
    private Util util = new Util();

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        ElementAvailability elementAvailability = ElementAvailability.Unavailable;
        int tries = 0;
        while (elementAvailability == ElementAvailability.Unavailable || tries < 10) {
            elementAvailability = ElementAvailability.from(the(Visibility.of(inboxPage.LBLSUBJECTRECEIVED).viewedBy(actor)));
            if (elementAvailability == ElementAvailability.Available) {
                tries = 10;
            }
            tries++;

            if (tries == 9) {
                actor.attemptsTo(
                        Click.on(InboxPage.BTNHIGHMAIL),
                        Click.on(InboxPage.BTNMAILBOX)
                );
            }
        }
        return elementAvailability;
    }

    public static MailReceived element() {
        return new MailReceived();
    }
}
