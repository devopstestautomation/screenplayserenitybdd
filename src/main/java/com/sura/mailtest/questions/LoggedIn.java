package com.sura.mailtest.questions;

import com.sura.mailtest.user_interfaces.InboxPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Visibility;

import static net.serenitybdd.screenplay.questions.ValueOf.the;

@Subject("La vista de bandeja de entrada")
public class LoggedIn implements Question<ElementAvailability> {

    @Override
    public ElementAvailability answeredBy(Actor actor) {
        return ElementAvailability.from(the(Visibility.of(InboxPage.BTNMENUGMAIL).viewedBy(actor)));
    }

    public static LoggedIn element() {
        return new LoggedIn();
    }
}
