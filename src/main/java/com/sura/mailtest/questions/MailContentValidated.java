package com.sura.mailtest.questions;

import com.sura.mailtest.models.Mail;
import com.sura.mailtest.user_interfaces.ReadMailPage;
import com.sura.mailtest.util.Util;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.questions.Text;

import static net.serenitybdd.screenplay.questions.ValueOf.the;

@Subject("El contenido del correo validado")
public class MailContentValidated implements Question<Mail> {

    private Util util = new Util();

    @Override
    public Mail answeredBy(Actor actor) {
        String senderMail = util.getValueBetween(the(Text.of(ReadMailPage.LBLFROMMAIL).viewedBy(actor)));
        String subject = the(Text.of(ReadMailPage.LBLSUBJECTMAIL).viewedBy(actor));
        String bodyMail = the(Text.of(ReadMailPage.LBLBODYMAIL).viewedBy(actor));
        return new Mail(senderMail, subject, bodyMail);
    }

    public static MailContentValidated mailInfo() {
        return new MailContentValidated();
    }
}
