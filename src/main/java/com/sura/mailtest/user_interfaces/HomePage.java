package com.sura.mailtest.user_interfaces;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://www.gmail.com")
public class HomePage extends PageObject {
}
