package com.sura.mailtest.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class LoginPage {
    public static Target TXTUSER = Target.the("campo texto usuario").
            locatedBy(".//input[@id='identifierId']");
    public static Target TXTPASSWORD = Target.the("campo texto contraseña").
            locatedBy(".//div[@id='password']//input");
    public static Target BTNNEXT = Target.the("boton siguiente").
            locatedBy(".//content[contains(.,'Siguiente')]/..");
}
