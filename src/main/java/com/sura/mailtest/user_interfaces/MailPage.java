package com.sura.mailtest.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class MailPage {
    public static Target TXTDESTINATARY = Target.the("campo texto destinatario correo").
            locatedBy(".//textarea[@name='to']");
    public static Target TXTSUBJECT = Target.the("campo texto asunto correo").
            locatedBy(".//input[@name='subjectbox']");
    public static Target TXTBODY = Target.the("campo texto cuerpo correo").
            locatedBy(".//div[@role='textbox']");
    public static Target BTNSEND = Target.the("boton enviar correo").
            locatedBy(".//div[@role='button' and contains(.,'Enviar')]");
}

