package com.sura.mailtest.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ReadMailPage {
    public static Target LBLFROMMAIL = Target.the("label correo emisor").
            locatedBy(".//span[@class='go']");
    public static Target LBLSUBJECTMAIL = Target.the("label Asunto").
            locatedBy(".//h2[@class='hP']");
    public static Target LBLBODYMAIL = Target.the("label cuerpo del correo").
            locatedBy(".//div/div/div[@dir='ltr']");
    public static Target BTNMAILMANU = Target.the("boton desplegar menu").
            locatedBy(".//img[@role='menu']");
    public static Target BTNDELETEMAIL = Target.the("boton borrar correo").
            locatedBy(".//div[contains(.,'Eliminar este mensaje')]/img");
}
