package com.sura.mailtest.user_interfaces;

import com.sura.mailtest.util.JsonController;
import net.serenitybdd.screenplay.targets.Target;

public class InboxPage {

    private JsonController jc = new JsonController();

    public static Target BTNMENUGMAIL = Target.the("boton menu gmail").
            locatedBy(".//a[@href='#inbox']");
    public static Target BTNREDACTMAIL = Target.the("boton redactar").
            locatedBy(".//div[@role='button' and contains(.,'REDACTAR')]");
    public static Target LBLSENTMAIL = Target.the("label notificacion correo enviado").
            locatedBy(".//div[contains(.,'Tu mensaje ha sido enviado.') or contains(.,'Se ha enviado el mensaje.')]");
    public Target LBLSUBJECTRECEIVED = Target.the("label correo recibido").
            locatedBy(".//span[@class='bog' and contains(.,'" + jc.getMailInfo("subject") + "')]");
    public static Target BTNMAILBOX = Target.the("boton bandeja entrada").
            locatedBy(".//a[contains(.,'Recibidos')]");
    public static Target BTNHIGHMAIL = Target.the("boton destacados").
            locatedBy(".//a[contains(.,'Destacados')]");
}