package com.sura.mailtest.tasks;

import com.sura.mailtest.user_interfaces.HomePage;
import com.sura.mailtest.user_interfaces.LoginPage;
import com.sura.mailtest.util.Security;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Login implements Task {

    private HomePage homePage;
    private final String gmailUser;
    private final String gmailPass;

    private Security security = new Security();

    public Login(String gmailUser, String gmailPass) {
        this.gmailUser = gmailUser;
        this.gmailPass = gmailPass;
    }

    @Step("{0} Inicia sesión en la aplicacion web de gmail con usuario: #gmailUser y contraseña: #gmailPass")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(homePage),
                Enter.theValue(gmailUser).into(LoginPage.TXTUSER),
                Click.on(LoginPage.BTNNEXT),
                WaitUntil.the(LoginPage.TXTPASSWORD, isVisible()),
                Enter.theValue(security.decodeBase64(gmailPass)).into(LoginPage.TXTPASSWORD),
                Click.on(LoginPage.BTNNEXT)
        );
    }

    public static Login withCredentials(String gmailUser, String gmailPass) {
        return instrumented(Login.class, gmailUser, gmailPass);
    }
}
