package com.sura.mailtest.tasks;

import com.sura.mailtest.user_interfaces.InboxPage;
import com.sura.mailtest.user_interfaces.MailPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;


public class SendMail implements Task {
    private final String destinatary;
    private final String subject;
    private final String body;

    public SendMail(String destinatary, String subject, String body) {
        this.destinatary = destinatary;
        this.subject = subject;
        this.body = body;
    }

    @Step("{0} envia correo a #destinatary con asunto: #subject y cuerpo: #body")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(InboxPage.BTNREDACTMAIL),
                Enter.theValue(destinatary).into(MailPage.TXTDESTINATARY).thenHit(Keys.TAB),
                Enter.theValue(subject).into(MailPage.TXTSUBJECT).thenHit(Keys.TAB),
                Enter.theValue(body).into(MailPage.TXTBODY),
                Click.on(MailPage.BTNSEND)
        );
    }

    public static SendMail withData(String destinatary, String subject, String body) {
        return instrumented(SendMail.class, destinatary, subject, body);
    }
}
