package com.sura.mailtest.tasks;

import com.sura.mailtest.user_interfaces.ReadMailPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class DeleteMail implements Task {
    @Step("{0} borra el correo recibido")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ReadMailPage.BTNMAILMANU),
                WaitUntil.the(ReadMailPage.BTNDELETEMAIL, isVisible()),
                Click.on(ReadMailPage.BTNDELETEMAIL)
        );
    }

    public static DeleteMail perform() {
        return instrumented(DeleteMail.class);
    }
}
