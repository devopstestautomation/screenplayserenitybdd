package com.sura.mailtest.tasks;

import com.sura.mailtest.user_interfaces.InboxPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ReadMail implements Task {

    private InboxPage inboxPage = new InboxPage();

    @Step("{0} selecciona el correo recibido en la bandeja de entrada")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(inboxPage.LBLSUBJECTRECEIVED)
        );
    }

    public static ReadMail perform() {
        return instrumented(ReadMail.class);
    }
}
