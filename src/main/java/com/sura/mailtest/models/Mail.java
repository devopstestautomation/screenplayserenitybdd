package com.sura.mailtest.models;

public class Mail {
    private final String senderMail;
    private final String subject;
    private final String body;

    public Mail(String senderMail, String subject, String body) {
        this.senderMail = senderMail;
        this.subject = subject;
        this.body = body;
    }

    public String getSenderMail() {
        return senderMail;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return String.format("{senderMail='%s', subject='%s', body='%s'}", senderMail, subject, body);
    }
}
