package com.sura.mailtest.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class JsonController {

    private JSONObject getJson(String fileName) {
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader(new File(fileName).getAbsolutePath()));
            return (JSONObject) obj;
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUserInfo(String rol, String key) {
        JSONObject data = getJson("src/test/resources/dataJson/dataUsers.json");
        assert data != null;
        JSONObject senderJson = (JSONObject) data.get(rol);
        return (String) senderJson.get(key);
    }

    public String getMailInfo(String key) {
        JSONObject data = getJson("src/test/resources/dataJson/dataMail.json");
        assert data != null;
        return (String) data.get(key);
    }
}
