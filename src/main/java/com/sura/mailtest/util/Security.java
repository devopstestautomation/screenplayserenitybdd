package com.sura.mailtest.util;

import org.apache.commons.codec.binary.Base64;

public class Security {
    public String decodeBase64(String key) {
        return new String(Base64.decodeBase64(key));
    }
}
