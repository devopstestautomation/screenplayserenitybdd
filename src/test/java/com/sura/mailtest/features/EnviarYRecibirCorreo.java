package com.sura.mailtest.features;

import com.sura.mailtest.models.Mail;
import com.sura.mailtest.models.User;
import com.sura.mailtest.questions.*;
import com.sura.mailtest.tasks.DeleteMail;
import com.sura.mailtest.tasks.Login;
import com.sura.mailtest.tasks.ReadMail;
import com.sura.mailtest.tasks.SendMail;
import com.sura.mailtest.util.JsonController;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.matchers.ConsequenceMatchers.displays;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SerenityRunner.class)
public class EnviarYRecibirCorreo {

    private JsonController jc = new JsonController();

    private Actor sender = Actor.named("emisor");
    private Actor receiver = Actor.named("receptor");
    private User senderUser = new User(
            jc.getUserInfo(sender.getName(), "mail"),
            jc.getUserInfo(sender.getName(), "password"));
    private User receiverUser = new User(
            jc.getUserInfo(receiver.getName(), "mail"),
            jc.getUserInfo(receiver.getName(), "password"));
    private Mail mail = new Mail(
            senderUser.getUsername(),
            jc.getMailInfo("subject"),
            jc.getMailInfo("body"));

    @Managed
    private WebDriver senderBrowser;
    @Managed
    private WebDriver receiverBrowser;

    @Before
    public void assignBrowsersToActors() {
        sender.can(BrowseTheWeb.with(senderBrowser));
        receiver.can(BrowseTheWeb.with(receiverBrowser));
    }

    @Test
    public void deberia_ser_capaz_de_enviar_y_recibir_correos() throws InterruptedException {
        givenThat(sender).wasAbleTo(Login.withCredentials(senderUser.getUsername(), senderUser.getPassword()));
        and(receiver).wasAbleTo(Login.withCredentials(receiverUser.getUsername(), receiverUser.getPassword()));
        then(sender).should(seeThat(LoggedIn.element(), is(ElementAvailability.Available)));
        when(sender).attemptsTo(SendMail.withData(receiverUser.getUsername(), jc.getMailInfo("subject"), jc.getMailInfo("body")));
        then(sender).should(seeThat(MailSent.element(), is(ElementAvailability.Available)));
        when(receiver).should(seeThat(MailReceived.element(), is(ElementAvailability.Available)));
        and(receiver).attemptsTo(ReadMail.perform());
        then(receiver).should(seeThat(MailContentValidated.mailInfo(),
                displays("senderMail", equalTo(mail.getSenderMail())),
                displays("subject", equalTo(mail.getSubject())),
                displays("body", equalTo(mail.getBody()))
        ));
        and(receiver).attemptsTo(DeleteMail.perform());
    }
}
