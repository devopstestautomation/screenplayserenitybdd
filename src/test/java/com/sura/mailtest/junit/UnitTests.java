package com.sura.mailtest.junit;

import com.sura.mailtest.util.Security;
import com.sura.mailtest.util.Util;
import org.junit.Assert;
import org.junit.Test;

public class UnitTests {
    private Security security = new Security();
    private Util util = new Util();

    @Test
    public void decodeBase64Test(){
        Assert.assertEquals("decodePrueba",security.decodeBase64("ZGVjb2RlUHJ1ZWJh"));
    }

    @Test
    public void getValueBetweenTest(){
        Assert.assertEquals("obtenerValor",util.getValueBetween("<obtenerValor>"));
    }
}
